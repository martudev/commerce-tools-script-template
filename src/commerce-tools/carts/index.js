export default ({ logger, commercetools }) => {
    const service = {};
    const { client, getRequestBuilder } = commercetools;
  
    service.create = async (...productIds) => {
      const requestBuilder = getRequestBuilder();

      const lineItems = productIds.map((productId) => {
          return { productId: productId }
      })
  
      return client
        .execute({
          uri: requestBuilder.carts.parse({}).build(),
          method: 'POST',
          body: {
              currency: 'USD',
              country: 'US',
              lineItems: lineItems,
              shippingAddress: {
                streetName: 'Caseros',
                streetNumber: '2075',
                postalCode: '1636',
                country: 'US'
              }
          }
        })
        .then(res => res.body);
    };
  
    return service;
  };
  