export default ({ logger, commercetools }) => {
  const service = {};
  const { client, getRequestBuilder } = commercetools;

  service.list = async () => {
    const requestBuilder = getRequestBuilder();

    return client
      .execute({
        uri: requestBuilder.products.parse({}).build(),
        method: 'GET',
      })
      .then(res => res.body);
  };

  service.search = async (value, lang) => {
    const requestBuilder = getRequestBuilder();

    return client
      .execute({
        uri: requestBuilder.productProjectionsSearch.text(value, lang).parse({}).build(),
        method: 'GET',
      })
      .then(res => res.body);
  };

  return service;
};
