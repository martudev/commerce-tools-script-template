export default ({ logger, commercetools }) => {
    const service = {};
    const { client, getRequestBuilder } = commercetools;
  
    service.create = async (cartId) => {
      const requestBuilder = getRequestBuilder();

      /*const lineItems = productIds.map((productId) => {
          return { productId: productId }
      })*/
  
      return client
        .execute({
          uri: requestBuilder.orders.parse({}).build(),
          method: 'POST',
          body: {
              cart: {
                  id: cartId
              },
              version: 1
          }
        })
        .then(res => res.body);
    };
  
    return service;
  };
  