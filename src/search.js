import App from './index'

const args = process.argv.slice(2);
const input = args[0]

App.products.search(input, 'en-US').then(data => console.log(data.results))