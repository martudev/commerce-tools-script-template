import Config from './config';
import Logger from './logger';
import Commercetools from './commerce-tools';
import Customers from './commerce-tools/customers';
import ListCustomers from './services/list-customers';
import Products from './commerce-tools/products';
import Carts from './commerce-tools/carts';
import Orders from './commerce-tools/orders';

const config = Config();

const logger = Logger({
  level: config.get('LOGGER:LEVEL'),
  isDisabled: Boolean(config.get('LOGGER:IS_DISABLED')),
});

const commercetools = Commercetools({
  clientId: config.get('COMMERCE_TOOLS:CLIENT_ID'),
  clientSecret: config.get('COMMERCE_TOOLS:CLIENT_SECRET'),
  projectKey: config.get('COMMERCE_TOOLS:PROJECT_KEY'),
  host: config.get('COMMERCE_TOOLS:API_HOST'),
  oauthHost: config.get('COMMERCE_TOOLS:OAUTH_URL'),
  concurrency: config.get('COMMERCE_TOOLS:CONCURRENCY'),
});

const customers = Customers({ logger, commercetools });

const products = Products({ logger, commercetools });

const carts = Carts({ logger, commercetools });

const orders = Orders({ logger, commercetools });

const listCustomers = ListCustomers({ logger, customers });


const app = {};

app.service = listCustomers;
app.products = products;
app.carts = carts;
app.orders = orders;

app.service.run();

export default app;